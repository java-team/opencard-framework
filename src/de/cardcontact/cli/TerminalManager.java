/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.cli;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Observable;

import opencard.core.service.CardIDFilter;
import opencard.core.terminal.CardID;
import opencard.core.terminal.CardTerminal;
import opencard.core.terminal.CardTerminalRegistry;

public class TerminalManager extends Observable implements CardIDFilter {

	private HashSet<String> ignored;
	private String selectedTerminal;
	private CardTerminalRegistry ctr;
	private ClientProperties clientProperties;


	public TerminalManager() {
		clientProperties = new ClientProperties();
		ctr = CardTerminalRegistry.getRegistry();
		ignored = clientProperties.getIgnoredReader();
		selectedTerminal = clientProperties.getReaderName();
	}



	@Override
	public boolean isCandidate(CardID cardID) {
		CardTerminal terminal = cardID.getCardTerminal();  
		HashSet<String> ignoreSet = clientProperties.getIgnoredReader();

		if (ignoreSet.contains(terminal.getName())) {
			return false;
		}

		return true;
	}



	public HashSet<String> getAllTerminals() {
		HashSet<String> all = getValidTerminals();
		all.addAll(ignored);
		return all;
	}



	/**
	 * {available reader} \ {ignored reader}
	 */
	public HashSet<String> getValidTerminals() {
		HashSet<String> valid = new HashSet<String>();
		Enumeration ctlist = ctr.getCardTerminals();

		while(ctlist.hasMoreElements()) {
			CardTerminal ct = (CardTerminal)ctlist.nextElement();
			String reader = ct.getName();
			if (!ignored.contains(reader)) {
				valid.add(reader);
			}
		}

		return valid;
	}



	public HashSet<String> getIgnoredTerminals() {
		return ignored;
	}



	public void setIgnoredTerminals(HashSet<String> ignored) {
		this.ignored = ignored;
	}



	public void ignoreTerminal(String terminal) {
		this.ignored.add(terminal);
	}



	public void approveTerminal(String terminal) {
		this.ignored.remove(terminal);
	}



	public String getSelectedTerminal() {
		return selectedTerminal;
	}



	public void setSelectedTerminal(String terminal) {
		selectedTerminal = terminal;
	}



	public void saveSettings() {
		clientProperties.saveIgnoredReader(ignored);
		clientProperties.saveReaderName(selectedTerminal);
		setChanged();
		notifyObservers();
	}



	public void discardChanges() {
		ignored = clientProperties.getIgnoredReader();
		selectedTerminal = clientProperties.getReaderName();
	}
}
