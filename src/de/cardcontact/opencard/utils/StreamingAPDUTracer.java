/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.opencard.utils;

import java.io.PrintStream;

import de.cardcontact.opencard.service.InstructionCodeTable;
import de.cardcontact.opencard.service.StatusWordTable;
import de.cardcontact.tlv.HexString;

import opencard.core.terminal.CardID;
import opencard.core.terminal.CommandAPDU;
import opencard.core.terminal.ResponseAPDU;
import opencard.core.terminal.SlotChannel;
import opencard.core.util.APDUTracer;



/**
 * Class implementing the APDUTracer interface to trace the content of a command
 * or response APDU into an associated stream. 
 * 
 */
public class StreamingAPDUTracer implements APDUTracer {

	PrintStream stream = null;

	/**
	 * Create trace object associated with stream
	 * 
	 * @param stream Stream to send trace information to
	 */
	public StreamingAPDUTracer(PrintStream stream) {
		this.stream = stream;
	}



	/**
	 * Compose a string describing the command APDU.
	 * 
	 * @param capdu the command APDU
	 * @return the string describing the command APDU
	 */
	public static String commandAPDUToString(CommandAPDU capdu) {
		StringBuffer sb = new StringBuffer(80);

		try {
			boolean extended = false;
			int len = capdu.getLength();
			byte[] buf = capdu.getBuffer();

			sb.append("C: ");
			sb.append(HexString.hexifyByteArray(buf, ' ', 4));
			sb.append(" - ");
			sb.append(InstructionCodeTable.instructionNameFromHeader(capdu.getBuffer()));

			len -= 4;
			int bodyoffset = 4;

			if (len > 0) {		// Case 2s, 2e, 3s, 3e, 4s, 4e
				int n = -1;

				if ((buf[bodyoffset] == 0) && (len > 1)) { // Extended length
					if (len >= 3) {	// Case 2e, 3e, 4e
						n = ((buf[bodyoffset + 1] & 0xFF) << 8) + (buf[bodyoffset + 2] & 0xFF);
						bodyoffset += 3;
						len -= 3;
						extended = true;
					} else {
						sb.append("Invalid extended length encoding for Lc\n");
						sb.append(HexString.dump(buf, bodyoffset, len, 16, 6));
					}
				} else {	// Case 2s, 3s, 4s
					n = buf[bodyoffset] & 0xFF;
					bodyoffset += 1;
					len -= 1;
				}

				if (len > 0) {	// Case 3s, 3e, 4s, 4e
					sb.append(" Lc=" + n + " " + (extended ? "Extended" : "") + "\n");
					if (n > len) {
						n = len;
					}
					sb.append(HexString.dump(buf, bodyoffset, n, 16, 6));
					bodyoffset += n;
					len -= n;

					n = -1;
					if (len > 0) {	// Case 4s, 4e 
						if (extended) {
							if (len >= 2) {
								n = ((buf[bodyoffset] & 0xFF) << 8) + (buf[bodyoffset + 1] & 0xFF);
								bodyoffset += 2;
								len -= 2;
							} else {
								sb.append("Invalid extended length encoding for Le\n");
								sb.append(HexString.dump(buf, bodyoffset, len, 16, 6));
							}
						} else {
							n = buf[bodyoffset] & 0xFF;
							bodyoffset += 1;
							len -= 1;
						}
					}
				}

				if (n >= 0) {
					sb.append("      Le=" + n + " " + (extended ? "Extended" : "") + "\n");
				}
				if (len > 0) {
					sb.append("Unexpected bytes:\n");
					sb.append(HexString.dump(buf, bodyoffset, len, 16, 6));
				}
			} else {
				sb.append("\n");
			}
		}
		catch(Exception e) {
			return "Error decoding APDU";
		}
		return sb.toString();
	}



	/**
	 * @see opencard.core.util.APDUTracer#traceCommandAPDU(opencard.core.terminal.SlotChannel, opencard.core.terminal.CommandAPDU)
	 */
	public void traceCommandAPDU(SlotChannel sc, CommandAPDU capdu) {

		int slotId = (sc.getCardTerminal().getName().hashCode() + sc.getSlotNumber()) & 0xFF;

		String s = HexString.hexifyByte(slotId);
		s = s.concat(" ");
		s = s.concat(commandAPDUToString(capdu));

		this.stream.print(s);
	}



	/**
	 * @see opencard.core.util.APDUTracer#traceResponseAPDU(opencard.core.terminal.SlotChannel, opencard.core.terminal.ResponseAPDU)
	 */
	public void traceResponseAPDU(SlotChannel sc, ResponseAPDU rapdu) {
		try {
			StringBuffer sb = new StringBuffer(80);
			int len = rapdu.getLength();
			byte[] buf = rapdu.getBuffer();

			sb.append("   R: ");
			sb.append(StatusWordTable.MessageForSW(rapdu.sw()));
			sb.append(" Lr=" + (len - 2));
			sb.append("\n");

			if (len > 2) {
				sb.append(HexString.dump(buf, 0, len - 2, 16, 6));
			}
			this.stream.print(sb.toString());
		}
		catch(Exception e) {
			this.stream.println("Error decoding APDU:");
			e.printStackTrace(this.stream);
		}
	}



	public void traceAnswerToReset(SlotChannel sc, CardID cardID) {
		this.stream.println(" ATR: " + HexString.hexifyByteArray(cardID.getATR()));
	}
}
