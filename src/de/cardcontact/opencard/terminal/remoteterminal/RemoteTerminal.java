/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.opencard.terminal.remoteterminal;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import de.cardcontact.opencard.service.remoteclient.RemoteProtocolUnit;

import opencard.core.terminal.CardID;
import opencard.core.terminal.CardTerminal;
import opencard.core.terminal.CardTerminalException;
import opencard.core.terminal.CommandAPDU;
import opencard.core.terminal.CommunicationErrorException;
import opencard.core.terminal.ResponseAPDU;
import opencard.core.terminal.SlotChannel;
import opencard.core.terminal.TerminalTimeoutException;
import opencard.core.util.Tracer;

public class RemoteTerminal extends CardTerminal {

	private final static Tracer ctracer = new Tracer(RemoteTerminal.class);

	private static final int timeout = 90;
	private CardID cardID = null;

	private LinkedBlockingQueue<RemoteProtocolUnit> comQueue = new LinkedBlockingQueue<RemoteProtocolUnit>(1);
	private LinkedBlockingQueue<RemoteProtocolUnit> resQueue = new LinkedBlockingQueue<RemoteProtocolUnit>(1);



	protected RemoteTerminal(String name, String type, String address) throws CardTerminalException {
		super(name, type, address);		
		ctracer.debug("RemoteTerminal", "TERMINAL: starting " + name);
		addSlots(1);
	}



	/**
	 * Transmit a command object and wait for a response object
	 * 
	 * @param cmdObject the command object (i.e. CommandAPDU, Reset, Notify)
	 * @return the response object (i.e. ResponseAPDU, CardID, Notify)
	 * @throws CardTerminalException
	 */
	protected RemoteProtocolUnit transmit(RemoteProtocolUnit cmdObject) throws CardTerminalException {
		RemoteProtocolUnit resObject;

		try {
			comQueue.put(cmdObject);

			ctracer.debug("transmit", "Waiting for response");

			resObject = resQueue.poll(timeout, TimeUnit.SECONDS);
			if (resObject == null) {
				throw new TerminalTimeoutException("The waiting time of " + timeout + " seconds for the response has expired.", timeout);
			}
			if (resObject.isClosing()) {
				throw new CommunicationErrorException(resObject.getMessage());
			}
		} catch (InterruptedException e) {
			throw new CardTerminalException(e.getMessage());
		}
		return resObject;
	}



	/**
	 * Poll for a command object. Used by remote connection.
	 * @return the command object (i.e. CommandAPDU, RemoteControl)
	 * @throws CardTerminalException
	 */
	public RemoteProtocolUnit poll(int timeout) throws CardTerminalException {
		RemoteProtocolUnit comObject;

		try {				
			ctracer.debug("poll", "TERMINAL: wait for available com apdu");
			ctracer.debug("poll", "TERMINAL: Queue size" + comQueue.size());
			comObject = comQueue.poll(timeout, TimeUnit.SECONDS);	
			ctracer.debug("poll", "TERMINAL: received com apdu");
			if (comObject == null) {
				throw new CommunicationErrorException("The waiting time of " + timeout + " seconds for the command apdu has expired.");
			}
		} catch (InterruptedException e) {
			throw new CardTerminalException(e.getMessage());
		}

		return comObject;
	}



	/**
	 * Put response object into queue. Used by remote connection.
	 * 
	 * @return the response object (i.e. ResponseAPDU, CardID, RemoteControl)
	 * @throws CardTerminalException
	 */
	public void put(RemoteProtocolUnit resObject) throws CardTerminalException {
		ctracer.debug("put", "Put response into queue");
		try {
			resQueue.put(resObject);
		} catch (InterruptedException e) {
			throw new CardTerminalException(e.getMessage());
		}
	}



	@Override
	protected void internalCloseSlotChannel(SlotChannel sc)
			throws CardTerminalException {

		ctracer.debug("internalCloseSlotChannel", "TERMINAL: Closing slot channel " + this.name);
	}



	@Override
	public CardID getCardID(int slotID) throws CardTerminalException {
		if (this.cardID != null) {
			return this.cardID;
		}
		return new CardID(this, 0, new byte[] {0x3b, (byte)0x80, 0x00, 0x00});
	}



	public void setCardID(CardID cardID) throws CardTerminalException {
		this.cardID = new CardID(this, 0, cardID.getATR());
	}



	@Override
	public boolean isCardPresent(int slotID) throws CardTerminalException {
		return true;
	}



	@Override
	public void open() throws CardTerminalException {
		ctracer.debug("open", "open");
	}



	@Override
	public void close() throws CardTerminalException {
		ctracer.debug("close", "close");

		if (!comQueue.isEmpty()) {
			ctracer.debug("close", "TERMINAL: clearing com queue...");
			comQueue.clear();
		}
		comQueue.offer(new RemoteProtocolUnit(RemoteProtocolUnit.Action.CLOSE));
	}



	@Override
	protected CardID internalReset(int slot, int ms) throws CardTerminalException {

		RemoteProtocolUnit rpu = transmit(new RemoteProtocolUnit(RemoteProtocolUnit.Action.RESET));

		if (!rpu.isRESET()) {
			throw new CardTerminalException("Received unexpected message");
		}

		setCardID((CardID)rpu.getPayload());
		return this.cardID;
	}



	@Override
	protected ResponseAPDU internalSendAPDU(int slot, CommandAPDU capdu, int ms)
			throws CardTerminalException {

		RemoteProtocolUnit rpu = transmit(new RemoteProtocolUnit(capdu));

		if (!rpu.isAPDU()) {
			throw new CardTerminalException("Received unexpected message");
		}

		return (ResponseAPDU)rpu.getPayload();
	}



	/**
	 * Send an asynchronous notification to the client
	 * 
	 * @param id the integer status code, < 0 denotes an error, 0 denotes completion
	 * @param message the message to be send
	 * @throws CardTerminalException
	 */
	public void sendNotification(int id, String message) throws CardTerminalException {

		comQueue.offer(new RemoteProtocolUnit(RemoteProtocolUnit.Action.NOTIFY, id, message));
	}
}
