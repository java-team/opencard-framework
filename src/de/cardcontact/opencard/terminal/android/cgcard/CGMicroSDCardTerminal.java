/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.opencard.terminal.android.cgcard;

import com.certgate.android.SmartCardJni;
import com.certgate.android.SmartCardVersion;

import opencard.core.terminal.CardID;
import opencard.core.terminal.CardTerminal;
import opencard.core.terminal.CardTerminalException;
import opencard.core.terminal.CommandAPDU;
import opencard.core.terminal.ResponseAPDU;
import opencard.core.util.HexString;
import opencard.core.util.Tracer;

/**
 * Class implementing a card terminal for Certgate micro SD card
 * 
 * @author Frank Thater
 */
public class CGMicroSDCardTerminal extends CardTerminal {

	private final static Tracer ctracer = new Tracer(CGMicroSDCardTerminal.class);

	private final static byte[] DUMMY_ATR = { 0x3B, (byte) 0xF8, 0x13, 0x00, 0x00, (byte) 0x81, 0x31, (byte) 0xFE, 0x45, 0x4A,
			0x43, 0x4F, 0x50, 0x76, 0x32, 0x34, 0x31, (byte) 0xB7 };

	private boolean connected = false;
	private String[] cardPaths;



	public CGMicroSDCardTerminal(String name, String type, String address, String[] cardPaths)
			throws CardTerminalException {
		super(name, type, address);
		this.cardPaths = cardPaths;

		SmartCardVersion version = SmartCardJni.getLibraryVersion();

		ctracer.debug("CGCardCardTerminal", "Creating terminal using library version " + version.Release + " " + version.Text);

		addSlots(1);
	}



	@Override
	public CardID getCardID(int slotID) throws CardTerminalException {
		// TODO: Adjust ATR dummy value
		return new CardID(DUMMY_ATR);
	}



	@Override
	public boolean isCardPresent(int slotID) throws CardTerminalException {
		// Log.d("CGCardCardTerminal", "Terminal JNI enumerateCards()");
		// boolean cardPresent = SmartCardJni.enumerateCards().length > 0; //
		// Resets the card - so do not use it
		ctracer.debug("CGCardCardTerminal", "Terminal isCardPresent()");
		return true;
	}



	@Override
	public void open() throws CardTerminalException {
		ctracer.debug("CGCardCardTerminal", "open()");

		int rc = SmartCardJni.openPaths(this.cardPaths);

		if (rc != 0) {
			ctracer.debug("CGCardCardTerminal", "Terminal open() failed " + rc);
			throw new CardTerminalException("Terminal open() failed " + rc);
		}

		connected = true;
	}



	@Override
	public void close() throws CardTerminalException {
		ctracer.debug("CGCardCardTerminal", "close()");

		int rc = SmartCardJni.close();

		if (rc != 0) {
			ctracer.debug("CGCardCardTerminal", "Terminal close() failed " + rc);
			connected = false;
			throw new CardTerminalException("Terminal close() failed " + rc);
		}

		connected = false;
	}



	@Override
	protected CardID internalReset(int slot, int ms) throws CardTerminalException {

		ctracer.debug("CGCardCardTerminal", "reset()");
		/* By now we only return a ATR dummy value as the real card ATR is not available using the JNI */
		return new CardID(DUMMY_ATR);
	}



	@Override
	protected ResponseAPDU internalSendAPDU(int slot, CommandAPDU capdu, int ms)
			throws CardTerminalException {
		if (!connected) {
			ctracer.debug("CGCardCardTerminal", "Terminal internalSendAPDU() - not connected");
		}

		ctracer.debug("CGCardCardTerminal", "sendAPDU()");
		ctracer.debug("CGCardCardTerminal",	"C-APDU: " + HexString.hexify(capdu.getBytes()));

		byte[] rsp = internalTransmit(capdu.getBytes());
		ctracer.debug("CGCardCardTerminal", "R-APDU: " + HexString.hexify(rsp));

		return new ResponseAPDU(rsp);
	}



	private byte[] internalTransmit(byte[] paramArrayOfByte) throws CardTerminalException {
		byte[] arrayOfByte = new byte[512];
		int i = SmartCardJni.apdu(paramArrayOfByte, arrayOfByte);
		if (i < 2) {
			ctracer.debug("CGCardCardTerminal", "Error transmitting APDU using JNI, rc = " + i);
			throw new IllegalStateException("internal error");
		}
		return shrinkByteArray(arrayOfByte, i);
	}



	private static byte[] shrinkByteArray(byte[] paramArrayOfByte, int paramInt) {
		byte[] arrayOfByte = new byte[paramInt];
		System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 0, paramInt);
		return arrayOfByte;
	}
}
