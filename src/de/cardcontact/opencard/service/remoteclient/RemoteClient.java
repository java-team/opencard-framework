/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.opencard.service.remoteclient;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import de.cardcontact.tlv.TLVDataSizeException;
import de.cardcontact.tlv.TLVEncodingException;
import de.cardcontact.tlv.TagSizeException;

import opencard.core.OpenCardException;
import opencard.core.service.CardChannel;
import opencard.core.service.CardService;
import opencard.core.service.CardServiceException;
import opencard.core.service.InvalidCardChannelException;
import opencard.core.service.SmartCard;
import opencard.core.terminal.CardID;
import opencard.core.terminal.CardTerminalException;
import opencard.core.terminal.CommandAPDU;
import opencard.core.terminal.ResponseAPDU;



/**
 * This client establish a connection to a remote administration server
 * and obtains command APDUs that will be transfered to the card.
 * The card's response will be send back to the server.
 * 
 * The APDUs are encoded according to RAMoverHTTP
 * 
 * @author lew
 */
public class RemoteClient extends CardService implements RemoteUpdateService {

	private HttpURLConnection connection;
	private String session = null;
	private String contentType = "application/org.openscdp-content-mgt-response;version=1.0";



	/**
	 * Read full binary contents
	 * 
	 * @param connection the http connection
	 * @return null if no data returned or a byte array containing the response
	 *
	 * @throws IOException
	 */
	private byte[] readFully(HttpURLConnection connection) throws IOException {
		int resplen = connection.getContentLength();

		if (resplen <= 0) {
			return null;
		}

		byte[] resp = new byte[resplen];
		InputStream is = connection.getInputStream();

		try	{
			int offset = 0;
			int bread = 0;
			while ((resplen > 0) && (bread = is.read(resp, offset, resplen)) != -1) {
				offset += bread;
				resplen -= bread;
			}
		}
		finally {
			is.close();
		}

		return resp;
	}



	/**
	 * Connect to the remote administration server
	 * 
	 * @param serverURL The url of the remote administration server
	 * @return the command scripting template
	 * @throws IOException 
	 * @throws TLVEncodingException 
	 */
	private byte[] initialConnect(String serverURL) throws IOException, TLVEncodingException {
		URL url = new URL(serverURL);
		connection = (HttpURLConnection) url.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", contentType);

		// Use the given sessionid if available
		if (session != null) {
			connection.addRequestProperty("Cookie", session);
		}

		RemoteProtocolEncoder rpe = new RemoteProtocolEncoder();
		rpe.add(new RemoteProtocolUnit(getCard().getCardID()));
		OutputStream writer = connection.getOutputStream();

		writer.write(rpe.encodeInitiationTemplate());
		writer.close();

		// The session returned from the server may differ from the given one
		String cookie = connection.getHeaderField("Set-Cookie");
		if (cookie != null) {
			session = cookie.split(";")[0];
		}

		byte[] data = readFully(connection);

		if (data == null) {
			throw new CardServiceException("No data received from server. HTTP code " + connection.getResponseCode() + " " + connection.getResponseMessage());
		}

		return data;
	}



	/**
	 * Send the Response Scripting Template to the server
	 * and obtain the next Command Scripting Template from the server
	 * if available.
	 * 
	 * @param serverURL The url of the remote administration server
	 * @param rst The response APDU that will be send to the server
	 * @return The new command APDU or null if it's not exists.
	 * @throws IOException
	 * @throws TLVEncodingException
	 * @throws TagSizeException
	 * @throws TLVDataSizeException
	 */
	private byte[] processNext(String serverURL, byte[] rst) throws IOException, TLVEncodingException, TagSizeException, TLVDataSizeException {
		URL url = new URL(serverURL);	

		connection = (HttpURLConnection) url.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.addRequestProperty("Cookie", session);
		connection.setRequestProperty("Content-Type", contentType);

		DataOutputStream writer = new DataOutputStream(connection.getOutputStream());

		writer.write(rst);
		writer.close();

		return readFully(connection);
	}



	private RemoteProtocolUnit sendCommandAPDU(RemoteProtocolUnit rpu) throws InvalidCardChannelException, CardTerminalException{
		ResponseAPDU res;
		CardChannel channel;

		CommandAPDU capdu = (CommandAPDU)rpu.getPayload();
		try {
			allocateCardChannel();
			channel = getCardChannel();
			res = channel.sendCommandAPDU(capdu);
		} finally {
			releaseCardChannel();
		}

		return new RemoteProtocolUnit(res);
	}



	private RemoteProtocolUnit resetCard(RemoteProtocolUnit rpu) throws CardTerminalException {
		SmartCard sc = getCard();
		CardID cid = sc.reset(false);

		if (cid == null) {
			throw new CardTerminalException("Could not reset card");
		}
		return new RemoteProtocolUnit(cid);
	}



	private byte[] process(byte[] cst, RemoteNotificationListener notificationListener) throws TLVEncodingException {
		RemoteProtocolEncoder rpe = new RemoteProtocolEncoder();

		rpe.decodeCommandScriptingTemplate(cst);
		List<RemoteProtocolUnit> rpus = rpe.getRemoteProtocolUnits();

		rpe = new RemoteProtocolEncoder();

		try	{
			for (RemoteProtocolUnit rpu : rpus) {
				switch(rpu.getAction()) {
				case APDU:
					rpe.add(sendCommandAPDU(rpu));
					break;
				case RESET:
					rpe.add(resetCard(rpu));
					break;
				case NOTIFY:
					if (notificationListener != null) {
						notificationListener.remoteNotify(rpu.getId(), rpu.getMessage());
					}
					break;
				}
			}
		} catch(OpenCardException cte) {
			rpe.add(new RemoteProtocolUnit(RemoteProtocolUnit.Action.CLOSE, -1, cte.getMessage()));
		}

		return rpe.encodeResponseScriptingTemplate();
	}



	@Override
	public void update(String serverURL, String sessionId, RemoteNotificationListener notificationListener) throws CardServiceException {

		if (sessionId != null) {
			session = "JSESSIONID=" + sessionId;
		}
		try {
			byte[] cst = initialConnect(serverURL);
			int code = connection.getResponseCode();
			while( code == 200) {
				byte[] rst = process(cst, notificationListener);
				cst = processNext(serverURL, rst);
				code = connection.getResponseCode();
			}
		} catch (CardServiceException e) {
			throw e;
		} catch (FileNotFoundException e) {
			throw new CardServiceException("URL " + serverURL + " not found");
		} catch (IOException e) {
			throw new CardServiceException("IO error during connection to " + serverURL + "(" + e.getMessage() + ")");
		} catch (TLVEncodingException e) {
			throw new CardServiceException(e.getMessage());
		} catch (TagSizeException e) {
			throw new CardServiceException(e.getMessage());
		} catch (TLVDataSizeException e) {
			throw new CardServiceException(e.getMessage());
		}
	}
}
