/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.opencard.service.smartcardhsm;

import java.math.BigInteger;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.ECPoint;

import de.cardcontact.tlv.ConstructedTLV;
import de.cardcontact.tlv.TLVEncodingException;
import opencard.opt.security.PrivateKeyRef;



/**
 * Reference to the private key on the SmartCardHSM
 * 
 * @author lew
 * 
 * @see opencard.opt.security.PrivateKeyRef
 *
 */
public class SmartCardHSMKey implements PrivateKeyRef {


	/**
	 * 
	 */
	private static final long serialVersionUID = -464439997111473313L;



	/**
	 * The ID which refers to the private key on the card.
	 */
	private byte keyID;



	private String label;



	private short keySize;



	private byte[] description;
	
	

	public SmartCardHSMKey(byte keyID, String label, short keySize) {
		this.keyID = keyID;
		this.label = label;
		this.keySize = keySize;
	}



	@Override
	public String getAlgorithm() {
		return "RSA";
	}



	public byte getKeyID() {
		return keyID;
	}



	public void setKeyID(byte keyID) {
		this.keyID = keyID;
	}



	public String getLabel() {
		return label;
	}



	public void setLabel(String label) {
		this.label = label;
	}



	public short getKeySize() {
		return keySize;
	}



	public void setKeySize(short keySize) {
		this.keySize = keySize;
	}



	@Override
	public byte[] getEncoded() {
		return null;
	}



	@Override
	public String getFormat() {
		return null;
	}



	/**
	 * Set key description and also set label and key size.
	 * @param description
	 */
	public void setDescription(byte[] description) {
		this.description = description;
		setLabelFromDescription();
		setKeySizeFromDescription(); 
	}



	private void setLabelFromDescription() {
		if (this.label == null || this.label.equals("")) {		
			ConstructedTLV descr = null;
			try {
				descr = new ConstructedTLV(this.description);
			} catch (TLVEncodingException e) {
				e.printStackTrace();
			}
			byte[] value = ((ConstructedTLV)descr.get(0)).get(0).getValue(); 
			String label = new String(value);
			setLabel(label);
		}			
	}

	
	
	/**
	 * Derive the key size from the certificate's public key
	 * 
	 * @param cert The corresponding certificate to this private key
	 */
	public void deriveKeySizeFromPublicKey(Certificate cert) {
		PublicKey pk = cert.getPublicKey();
		byte[] component;
		
		if (pk instanceof RSAPublicKey) {
			RSAPublicKey rsaPK = (RSAPublicKey)pk;
			component = rsaPK.getModulus().toByteArray();
		} else if (pk instanceof ECPublicKey) {
			ECPublicKey ecPK = (ECPublicKey)pk;
			ECPoint w = ecPK.getW();
			component = w.getAffineX().toByteArray();
		} else {
			return;
		}
		
		if (component[0] == 0) { // Remove sign bit
			setKeySize((short) ((component.length - 1) * 8));
		} else {
			setKeySize((short) (component.length * 8));
		}				
	}
	
	

	/**
	 * Extract the key size information from the private key description if available.
	 * If the key size property is missing then the key size is set to -1. 
	 */
	private void setKeySizeFromDescription() {
		ConstructedTLV descr = null;
		byte[] value = null;
		
		try {
			descr = new ConstructedTLV(this.description);
			de.cardcontact.tlv.Tag tagA1  = new de.cardcontact.tlv.Tag(0xA1);
			//value = ((ConstructedTLV) ((ConstructedTLV)descr.findTag(tagA1, null)).get(0)).get(1).getValue();
			
			ConstructedTLV tlv = (ConstructedTLV) descr.findTag(tagA1, null);
			tlv = (ConstructedTLV) tlv.get(0);
			tlv = (ConstructedTLV) tlv.get(1);
			value = tlv.getValue();
					
			BigInteger keysize = byteArrayToUnsignedBigInteger(value);
			this.keySize = keysize.shortValue();
		} catch (Exception e) {
			this.keySize = -1;
		}
	}



	private BigInteger byteArrayToUnsignedBigInteger(byte[] data) {
		byte[] absoluteValue = new byte[data.length + 1];
		System.arraycopy(data, 0, absoluteValue, 1, data.length);
		return new BigInteger(absoluteValue);
	}



	@Override
	public String toString() {
		return "Label=" + label + ", KeyID=" + keyID + ", Size=" + keySize + " bits"; 
	}
}
