/*
 * Copyright (c) 2016 CardContact Systems GmbH, Minden, Germany.
 *
 * Redistribution and use in source (source code) and binary (object code)
 * forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 1. Redistributed source code must retain the above copyright notice, this
 * list of conditions and the disclaimer below.
 * 2. Redistributed object code must reproduce the above copyright notice,
 * this list of conditions and the disclaimer below in the documentation
 * and/or other materials provided with the distribution.
 * 3. The name of CardContact may not be used to endorse or promote products derived
 * from this software or in any other form without specific prior written
 * permission from CardContact.
 * 4. Redistribution of any modified code must be labeled "Code derived from
 * the original OpenCard Framework".
 *
 * THIS SOFTWARE IS PROVIDED BY CardContact "AS IS" FREE OF CHARGE. CardContact SHALL NOT BE
 * LIABLE FOR INFRINGEMENTS OF THIRD PARTIES RIGHTS BASED ON THIS SOFTWARE.  ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  CardContact DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THIS
 * SOFTWARE WILL MEET THE USER'S REQUIREMENTS OR THAT THE OPERATION OF IT WILL
 * BE UNINTERRUPTED OR ERROR-FREE.  IN NO EVENT, UNLESS REQUIRED BY APPLICABLE
 * LAW, SHALL CardContact BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  ALSO, CardContact IS UNDER NO OBLIGATION
 * TO MAINTAIN, CORRECT, UPDATE, CHANGE, MODIFY, OR OTHERWISE SUPPORT THIS
 * SOFTWARE.
 */

package de.cardcontact.opencard.service.smartcardhsm;

import java.io.IOException;
import java.nio.ByteBuffer;

import de.cardcontact.tlv.ConstructedTLV;
import de.cardcontact.tlv.PrimitiveTLV;
import de.cardcontact.tlv.TLVEncodingException;



/**
 * This class contains the data for EC key pair generation. 
 * 
 * @author lew
 *
 */
public class SmartCardHSMRSAPrivateKeySpec extends SmartCardHSMPrivateKeySpec {
	
	
	
	private int exponent;
	
	
	
	private int modulusSize;
	
	
	
	/**
	 * SmartCardHSMRSAPrivateKeySpec constructor
	 *
	 * @param car The Certificate Authority Reference
	 * @param chr The Certificate Holder Reference
	 * @param algorithm The key algorithm
	 * @param params The domain parameter
	 */
	public SmartCardHSMRSAPrivateKeySpec(String car, String chr, int exponent, int size) {
		super(car, chr);
		this.exponent = exponent;
		this.modulusSize = size;
	}
	
	
	
	/**
	 * The command data for RSA key pair generation.
	 * @return The TLV encoded c-data
	 */	
	public byte[] getCData() throws IOException, TLVEncodingException {
		
		ConstructedTLV gakpcdata = new ConstructedTLV(0x30);
		
		//CPI
		byte[] cpi = {getCpi()};							
		gakpcdata.add(new PrimitiveTLV(0x5F29, cpi));
		
		//CAR
		if (hasCar()) {
			gakpcdata.add(new PrimitiveTLV(0x42, getCar()));
		}
		
		//Public Key
		ConstructedTLV puk = new ConstructedTLV(0x7F49);
		
		//Public Key Algorithm
		puk.add(new PrimitiveTLV(0x06, getAlgorithm()));
		
		
		//Public exponent
		byte[] exp = intToByteArray(exponent);
		puk.add(new PrimitiveTLV(0x82, exp));												
		
		//Key size
		puk.add(new PrimitiveTLV(0x02, intToByteArray(modulusSize)));
		
		//CHR
		gakpcdata.add(puk);
		gakpcdata.add(new PrimitiveTLV(0x5f20, getCertificateHolderReference()));
		
		//Outer Certificate Authority Reference for authentication signature if P2 != '00'
		if (hasOuterCar()) {								
			gakpcdata.add(new PrimitiveTLV(0x45, getOuterCar()));
		}
		
		if (hasKeyUseCounter()) {
			ByteBuffer buffer = ByteBuffer.allocate(4);
			buffer.putInt(getKeyUseCounter());
			byte[] bytes = buffer.array();
			
			gakpcdata.add(new PrimitiveTLV(0x90, bytes));
		}
		
		if (hasAlgorithmList()) {
			gakpcdata.add(new PrimitiveTLV(0x91, getAllowedAlgorithmList()));
		}
		
		return gakpcdata.getValue();
	}
	
	
	
	/**
	 * @return The size of the modulus
	 */
	public int getModulusSize() {
		return modulusSize;
	}

	/**
	 * @param modulusSize
	 */
	public void setModulusSize(int modulusSize) {
		this.modulusSize = modulusSize;
	}
	
	
	/** 
	 * @param num
	 * @return A new byte[] containing the number
	 */
	private static byte[] intToByteArray(int num) {
		double countBits = (Math.log(num) / Math.log(2));
		int length = (int) Math.round((countBits / 8) + 0.5);
		
		byte[] b = new byte[length];
		int j = length;
		for (int i = 0; i < length; i++) {
			int shift = 8 * --j;
			b[i] = (byte)(num >> shift);
		}
		
		return b;
	}
}
