package com.certgate.android;

public class SmartCardJni {
	static {
		System.loadLibrary("sdx");
		System.loadLibrary("sdx_jni");
	}



	public static native int apdu(byte[] input, byte[] output);

	public static native int close();

	public static native int[] enumerateCards();

	public static native SmartCardVersion getLibraryVersion();



	public static native String getReturncodeText(int returnCode);

	public static native int open();



	public static native int openPaths(String[] cardPaths);

	public static native int waitForStatusChange(int paramInt);
}
